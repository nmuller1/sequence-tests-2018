# Fichier makefile du projet3 de langage de programmation
# Noëmie Muller
# 000458865
# 06/04/2018
# Jacopo De Stefani

all: projet

projet: ValidateSequence.o SequenceTests.cpp
	g++ -std=c++17 -masm=intel -ggdb3 -Wpedantic -Wall -Wextra -Wconversion -Wsign-conversion -Weffc++ -Wstrict-null-sentinel -Wold-style-cast -Wnoexcept -Wctor-dtor-privacy -Woverloaded-virtual -Wsign-promo -Wzero-as-null-pointer-constant -Wsuggest-final-types -Wsuggest-final-methods -Wsuggest-override -m32 ValidateSequence.o SequenceTests.cpp -o Projet

ValidateSequence.o: ValidateSequence.asm
	nasm -f elf32 -g ValidateSequence.asm
