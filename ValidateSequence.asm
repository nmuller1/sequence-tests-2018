;; Muller Noëmie
;; 000458865
;; Langages de Programmation info-f-105
;; 06/04/2018

;; FUNCTION PROTOTYPE:
;; bool validate_sequence(unsigned char message[], unsigned sequence);

;; DESCRIPTION:
;; The function generates a sequence from the message passed as parameters and checsk if it corresponds to the sequence passed as second parameter
;; The return value is stored in the eax register (0/1))

;; PARAMETERS:
;;   [ebp+8] : Address of the message array - 32 bits
;;   [ebp+12]: Hash to verify - 32 bits

;; VARIABLES: The registers have the following uses:
;;   ...

CPU     586
GLOBAL  validate_sequence
EXTERN  project
GLOBAL  check
SECTION .text
validate_sequence:
        push    ebp                  ; Function prologue - Store value of ebp on the stack
        mov     ebp, esp             ; and copy esp into ebp (since esp could change during execution)
        
        push    ebx                  ; Push all the value of the registers to restore them in the end
        push    edx
        push    ecx
        push    edi
        push    esi
        
        mov     edi, [ebp+8]        ; Copy the adress of the message array in edi
        mov     eax,0               ; Init checksum
        mov     esi,0               ; Init counter of checked blocs

mainLoop:
        mov     ebx, [edi+esi*4]    ; Put the next bloc in ebx
        
        test    ebx, 0x000000FF     ; Check if the terminator is in the current bloc
        jz      projection
        test    ebx, 0x0000FF00
        jz      zerois3rd
        test    ebx, 0x00FF0000
        jz      zerois2nd
        test    ebx, 0xFF000000
        jz      zerois1st
        
        xor     eax,ebx             
        rol     eax,8
        inc     esi       

        cmp     esi,64              ; Check if we reach the maximum size of the message
        jz      projection

        jmp     mainLoop            ; Get the next bloc as long as the message isn't over

zerois3rd:
        and     ebx,0x000000FF      ; Masks the characters after the terminator
        xor     eax,ebx
        jmp     projection

zerois2nd:
        and     ebx,0x0000FFFF
        xor     eax,ebx
        jmp     projection

zerois1st:
        xor     eax,ebx             ; Doesn't need anymask, the zero is the last character

projection :
        push    eax                 ; Checksum is the parameter of the called 'project' function
        call    project
        add     esp,4

        cmp     eax, [ebp+12]       ; Compare hash with the return of the called function
        mov     eax, 0
        jne     end                 
        
ok:
        mov     eax,1               ; If the return of the called function == hash
end:
        pop     esi                 ; Restore all the registers
        pop     edi
        pop     ecx
        pop     edx
        pop     ebx

        mov     esp, ebp            ; Function epilogue - Restore esp and ebp old values
        pop     ebp
        ret